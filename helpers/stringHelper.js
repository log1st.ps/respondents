export const ucFirst = (str) => str[0].toUpperCase() + str.substr(1);

export const stringHelperMixin = {
  methods: {
    ucFirst,
  },
};
