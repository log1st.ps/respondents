export default ({ store }) => {
  if (!process.browser) {
    return;
  }

  const WorkerHandler = require('../worker/WorkerHandler').default;
  const worker = new WorkerHandler();

  store.sendMessage = (command, params, handler) => worker.message({
    command,
    params,
  }, handler);
};
