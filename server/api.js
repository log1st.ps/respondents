import apiStub from './api/post-respondents-form';

const express = require('express');

const app = express();
app.use(express.json());

app.post('/respondents-form', (req, res) => {
  res.json(apiStub(req.body));
});

export default app;
