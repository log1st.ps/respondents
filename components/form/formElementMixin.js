export default {
  props: {
    value: String,
    placeholder: String,
  },
  data() {
    return {
      isFocused: false,
      val: this.value,
    };
  },
  watch: {
    val: {
      deep: true,
      handler(val) {
        this.$emit('input', val);
      },
    },
    value: {
      deep: true,
      handler(value) {
        this.val = value;
      },
    },
  },
  methods: {
    focus() {
      this.isFocused = true;

      if (this.$refs.input) {
        this.$refs.input.focus();
      }
    },
    blur() {
      this.isFocused = false;
    },
  },
};
