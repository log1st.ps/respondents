/**
 * @param url
 * @param handler
 */
export default ({ url }, handler) => {
  const ws = new WebSocket(url);

  ws.onmessage = function ({ data }) {
    handler.postMessage(JSON.parse(data));
  };
};
