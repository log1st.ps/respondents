import axios from 'axios';

export default async ({ url, query, data }) => (await axios.post(url, data, {
  params: query,
})).data;
