import ExampleWorker from './Service.worker';

export default function Worker() {
  /**
   * @type Worker
   */
  const worker = new ExampleWorker();

  return Object.seal({
    inUse: false,
    async message({
      command,
      params,
    }, handler = null) {
      return new Promise((resolve) => {
        const channel = new MessageChannel();
        channel.port1.onmessage = (e) => {
          resolve(e.data);
        };

        const handlerChannel = new MessageChannel();
        if (handler) {
          handlerChannel.port1.onmessage = (e) => {
            handler(e.data);
          };
        }

        worker.postMessage(
          {
            command,
            params,
          },
          [channel.port2, handlerChannel.port2],
        );
      });
    },
  });
}
