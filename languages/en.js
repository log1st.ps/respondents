export default {
  navigation: {
    quizzes: 'Опросы',
    users: 'Пользователи',
    blacklists: 'Чёрные списки',
    callCenter: 'Колл-центр',
  },
  breadcrumbs: {
    quizzes: 'Опросы',
    'quizzes-add': 'Добавить опрос',
    users: 'Пользователи',
    blacklists: 'Чёрные списки',
    callCenter: 'Колл-центр',
  },
  roles: {
    admin: 'Администратор',
  },
  pages: {
    quizzes: {
      add: {
        params: 'Параметры',
        questions: 'Вопросы',
        logic: 'Логика',
        conditions: 'Условия',
        respondents: 'Респонденты',
      },
    },
  },
  forms: {
    respondents: {
      title: 'Добавить опрос',
      condition: 'Условие {n}',
      and: 'И',
      or: 'Или',
      fieldPlaceholder: 'Выберите условие',
      types: {
        range: 'Диапазон',
      },
      typeLabel: '{type} {n}',
      addType: 'Добавить {type}',

      fields: {
        age: {
          label: 'Возраст респондента',
          short: 'Возраст',
        },
        cardType: {
          label: 'Тип карты лояльности',
          short: 'Тип',
          values: {
            silver: 'Silver',
            gold: 'Gold',
            platinum: 'Platinum',
          },
        },
        cardStatus: {
          label: 'Статус карты лояльности',
          short: 'Статус',
          values: {
            disabled: 'Неактивна',
            enabled: 'Активна',
          },
        },
      },
      from: 'От',
      to: 'До',
      removeCondition: 'Удалить условие',
      addCondition: 'Нажмите, чтобы добавить новое условие выборки.\nВсе условия связываются между собой логическим "И"',
      test: 'Протестировать опрос',
      next: 'Далее',
    },
  },
  fields: {
    range: {
      from: 'От',
      to: 'До',
    },
  },
  header: {
    search: 'Поиск по системе',
  },
};
