import en from './languages/en';

module.exports = {
  head: {
    title: 'Respondents',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'Respondents test task' },
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic',
      },
    ],
  },
  serverMiddleware: [
    {
      path: '/api',
      handler: '~/server/api.js',
    },
  ],
  loading: { color: 'rgb(63, 81, 181)' },
  plugins: [
    '~plugins/bus',
    '~plugins/fragment',
  ],
  modules: [
    'nuxt-vuex-localstorage',
    'nuxt-svg-loader',
    [
      'nuxt-i18n',
      {
        strategy: 'no_prefix',
        locales: ['en'],
        defaultLocale: 'en',
        vueI18n: {
          fallbackLocale: 'en',
          messages: {
            en,
          },
        },
      },
    ],
  ],
  buildModules: [
    '@nuxtjs/dotenv',
  ],
  server: {
    port: 3004,
  },
  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }

      config.output.globalObject = 'this';

      if (isClient) {
        config.module.rules.unshift({
          test: /\.worker\.js$/,
          loader: 'worker-loader',
          exclude: /(node_modules)/,
          options: {
            inline: true,
          },
        });
      }
    },
  },
};
