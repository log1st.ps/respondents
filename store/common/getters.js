export const language = ({ language: value }) => value;

export const user = ({ user: value }) => value;

export const breadcrumbs = ({ breadcrumbs: value }) => [
  {
    key: 'home',
    icon: 'home',
    noLabel: true,
    url: { name: 'index' },
  },
  ...value,
];
