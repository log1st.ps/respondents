export const setLanguage = (state, value) => {
  state.language = value;
};

export const setBreadcrumbs = (state, breadcrumbs) => {
  state.breadcrumbs = breadcrumbs;
};
