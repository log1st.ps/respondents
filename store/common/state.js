export default () => ({
  language: 'en',
  user: {
    name: 'Александр С.',
    role: 'admin',
    image: require('../../tmp/user.jpg'),
  },
  breadcrumbs: [],
});
