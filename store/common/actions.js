const apiUrl = process.env.API_URL;

export async function sendRespondentsForm(store, model) {
  const { sendMessage } = this;
  const response = await sendMessage('post', {
    url: `${apiUrl}/respondents-form`,
    data: model,
  });

  console.log(response);
}
